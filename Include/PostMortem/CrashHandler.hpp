/*
 * PostMortemFramework
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef POSTMORTEM_TERMINATEHANDLER_HPP
#define POSTMORTEM_TERMINATEHANDLER_HPP

#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <cstring>
#include <exception>
#include <stdexcept>

#include <execinfo.h>
#include <signal.h>
#include <unistd.h>

namespace PostMortem
{

void printStacktrace()
{
    void* array[200];
    int size = backtrace(array, sizeof(array) / sizeof(array[0]));
    backtrace_symbols_fd(array, size, STDERR_FILENO);
}

void signalHandler(int sig)
{
    std::fprintf(stderr, "Error: signal %d - %s\n", sig, strsignal(sig));
    printStacktrace();
    std::_Exit(EXIT_FAILURE);
}

void terminateHandler()
{
    std::exception_ptr exptr = std::current_exception();
    if (exptr)
    {
        // the only useful feature of std::exception_ptr is that it can be rethrown...
        try
        {
            std::rethrow_exception(exptr);
        }
        catch (std::exception &ex)
        {
            std::fprintf(stderr, "Terminated due to exception: %s\n", ex.what());
        }
        catch (...)
        {
            std::fprintf(stderr, "Terminated due to unknown exception\n");
        }
    }
    else
    {
        std::fprintf(stderr, "Terminated due to unknown reason :(\n");
    }
    printStacktrace();
    std::_Exit(EXIT_FAILURE);
}

void registerCrashHandler()
{
    // register signal handlers
    signal(SIGABRT, signalHandler);
    signal(SIGFPE, signalHandler);
    signal(SIGILL, signalHandler);
    signal(SIGSEGV, signalHandler);
    // register exception handlers
    std::set_terminate(terminateHandler);
}

} // namespace PostMortem

#endif // POSTMORTEM_TERMINATEHANDLER_HPP
